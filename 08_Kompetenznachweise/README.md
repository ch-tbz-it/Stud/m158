# Kompetenznachweise – Modul 158

> #### LB1 - Theorietest
>
> Die erste Leistungsbeurteilung ist eine theoretische Prüfung. Die Inhalte für die LB1 stammen aus den theoretischen Aufträgen, den Übungen und dem Glossar.
>
> ( Erreichte Punkte / Maximale Punkte ) x 5 + 1 = Note LB1 (0.1 gerundet)
>
> ### Gewichtung 1/3
>
> Themen:
>
> - [Glossar](https://gitlab.com/ch-tbz-it/Stud/m158/-/blob/main/04_Unterrichtsressourcen/Glossar.md?ref_type=heads)
> - [SAAS, PAAS, IAAS, OnSite](https://gitlab.com/ch-tbz-it/Stud/m158/-/blob/main/04_Unterrichtsressourcen/03_%C3%9Cbungen/IaasPaasSaas/iaas-paas-saas-diagram.png?ref_type=heads)
> - [Präsentation Berechtigungen unter Linux](https://gitlab.com/ch-tbz-it/Stud/m158/-/tree/main/04_Unterrichtsressourcen/01_Pr%C3%A4sentationen?ref_type=heads)
> - [Übung System- und UNC-Pfade](https://gitlab.com/ch-tbz-it/Stud/m158/-/tree/main/04_Unterrichtsressourcen/03_%C3%9Cbungen?ref_type=heads)
> - [Unterschiede Pfade Linux und Windows](https://gitlab.com/ch-tbz-it/Stud/m158/-/blob/main/04_Unterrichtsressourcen/Unterschiede_Pfade_Windows_Linux.md?ref_type=heads)
> - [Übung CMS-DB-connect](https://gitlab.com/ch-tbz-it/Stud/m158/-/tree/main/04_Unterrichtsressourcen/03_%C3%9Cbungen/CMS-DB-connect?ref_type=heads)

<hr>



> #### [LB2 - Projekt Migration](https://gitlab.com/ch-tbz-it/Stud/m158/-/tree/main/02_Projekt%20Migration)
>
> WordPress-Web-App-Migration
>
> ( Erreichte Punkte / Maximale Punkte ) x 5 + 1 = Note LB2 (0.1 gerundet)
> 
>### Gewichtung 2/3

<hr>



> ### Modulnote
>
> (LB1 x 0.3334) + (LB2 x 0.667) = Modulnote (0.5 gerundet)

