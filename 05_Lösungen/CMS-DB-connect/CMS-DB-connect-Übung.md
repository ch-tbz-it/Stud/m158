# Übungen CMS-DB Connection

Verstehen Sie, wie und wann CMS-Applikationen eine Verbindung zu Datenbanken herstellen und wie der Ablauf vom Aufruf einer Webanwendung bis zur Datenbankabfrage ist.



## Übung 1

![Übung 1](https://gitlab.com/ch-tbz-it/Stud/m158/-/raw/main/05_L%C3%B6sungen/CMS-DB-connect/L%C3%B6sung%20Super-CMS.png?ref_type=heads)

> Erklären Sie in 9 Schritten was passiert, wenn eine Benutzerin oder ein Benutzer das CMS https://supercms.ch im Browser öffnet (ohne DNS). 
>
> Versuchen Sie pro Schritt eine Verbindung in der Grafik zu machen.

<div style="color:red"><ol>
    <li>Der Benutzer öffnet das CMS im Browser und eine HTTPS-Verbindung zum Webserver wird aufgebaut</li>
    <li>Der Webserver identifiziert das konfigurierte "Document Root", damit das CMS geladen werden kann</li>
    <li>Ein DB-Connect wird im CMS über mysqli gestartet</li>
    <li>DB-Verbindungseigenschaften werden ausgelesen</li>
    <li>Verbindungseigenschaften werden in Variablen gespeicheert</li>
    <li>DB-Abfrage erfolgt über mysqli</li>
    <li>Antwort des DB-Server wenn erfolgreiche Verbindung</li>
    <li>Antwort wird in Variablen gespeichert und vom CMS verarbeitet</li>
    <li>CMS sendet Antwort an Webserver</li>
    <li>Antwort an User Ausgabe im Browser</li>
    </ol></div>

## Übung 2

Überlegen Sie wo die folgenden Technologien/Protokolle in der Grafik im Einsatz sind

- https

  Schritte: 1 und 10

- mysqli

  Schritte: 6 und 7

- ext4

  Schritte: 2,3,4,5,8,9

und tragen Sie dies bei den passenden Verbindungen ein.



## Übung 3

Passen Sie die DB-Config Datei auf die Umgebung aus dem Bild an. Speichern Sie die angepasste Config-Datei ins Repository

```php
<?php

// This is the database connection configuration.
return array(
	//'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
	// uncomment the following lines to use a MySQL database

	'connectionString' => 'mysql:host=192.168.22.10:3306;dbname=superCMS', //Port ist nicht zwingend notwendig wenn Standard
	'emulatePrepare' => true,
	'username' => 'sCMS',
	'password' => 'xYzzAB23!',
	'charset' => 'utf8',

);
```

