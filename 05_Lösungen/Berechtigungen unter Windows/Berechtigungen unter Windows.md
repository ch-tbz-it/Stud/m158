# Berechtigungen unter Windows



## Ordnerstruktur erstellen

1. Erstellen Sie einen Ordner "C:\Mittelerde\"
2. Erstellen Sie einen Unterordner "C:\Mittelerde\Völker"
3. Erstellen Sie einen Unterordner "C:\Mittelerde\\Völker\Zwerge"
4. Erstellen Sie einen Unterordner "C:\Mittelerde\\Völker\Elben"
5. Erstellen Sie einen Unterordner "C:\Mittelerde\\Völker\Menschen"



## Gruppe erstellen und Vererbung erkennen

1. Fügen Sie eine neue Gruppe mit dem Namen "Menschen" zum Ordner "C:\Mittelerde" hinzu

2. Welche Änderungen können Sie bei den Ordner "Völker", Elben", Menschen" und "Zwerge" feststellen?

   <span style="color:red">Die Ordner "Völker", Elben", Menschen" und "Zwerge" erben die Gruppe "Menschen"</span>



## Vererbungskette erkennen

<img align="left" width="auto" height="auto" src="https://gitlab.com/ch-tbz-it/Stud/m158/-/raw/main/04_Unterrichtsressourcen/03_%C3%9Cbungen/Berechtigungen%20unter%20Windows/Struktur.png?ref_type=heads">

1. Von wo erben "Elben", "Menschen" und "Zwerge" in Windows mit dieser Konfiguration standardmässig die Berechtigungen?

   <span style="color:red">"C:\\"</span>

2. Von wo erbt "Völker" in Windows mit dieser Konfiguration standardmässig die Berechtigungen?

   <span style="color:red">"C:\\"</span>

3. Von wo erbt "Mittelerde" in Windows mit dieser Konfiguration standardmässig die Berechtigungen?

   <span style="color:red">"C:\\"</span>

4. Wenn ich bei Mittelerde die Vererbung deaktiviere, was ändert sich bei der Vererbung der Berechtigungen bei "Völker"?

   <span style="color:red">Völker erbt neu von C:\\Mittelerde</span>

5. Wenn ich bei Mittelerde die Vererbung deaktiviere, was ändert sich bei der Vererbung der Berechtigungen bei "Elben", "Menschen" und "Zwerge"?

   <span style="color:red">"Elben", "Menschen" und "Zwerge" erben neu von C:\\Mittelerde</span>



## Auswirkungen beim deaktivieren von Vergebungen verstehen

1. Sie möchten nun die Gruppe "Menschen" beim Ordner "Elben" und "Zwerge" entfernen. Was müssen Sie tun?

   <span style="color:red">Die Vererbung deaktivieren auf den Ordner "Elben" und "Zwerge" deaktivieren</span>

2. Wenn Sie eine Vererbung auf einem Ordner ausschalten, erhalten Sie eine Frage mit zwei Optionen. Erklären Sie die beiden Optionen.

   <span style="color:red">Wenn Sie die Vererbung ausschalten, muss Windows wissen, was mit den bestehenden Berechtigungen gemacht werden soll. Option 1: Die Berechtigungen in "eigenständige" nicht geerbete Berechtigungen umwandeln oder Option 2: Die Berechtigungen löschen. </span>

3. Wo kann ich im GUI bei Windows sehen, von wo ein Ordner seine Berechtigungen erbt?

   <span style="color:red">Bei den Eigenschaften eines Ordners unter Sicherheit unter Erweitert hat es ein Tab Berechtigungen und dort  findet sich eine Spalte "geerbet von"</span>

4. Warum reichte es nicht bei Mittelerde die Vererbung zu deaktivieren um die Gruppe "Menschen" bei "Elben" und "Zwerge" zu entfernen?

   <span style="color:red">Die Vererbungseinstellung bezieht sich immer auf den Ordner auf welchem diese deaktiviert wird. Wenn Sie bei "Mittellerde" die Vererbung deaktivieren, ist bei den Unterordner die Vererbung trotzdem noch aktiv </span>

