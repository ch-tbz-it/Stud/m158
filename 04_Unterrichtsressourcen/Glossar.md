# Glossar

| Wort                          | Bedeutungserklärungen                                        |
| ----------------------------- | ------------------------------------------------------------ |
| Migration                     | Umstellungsprozesse in Datenverarbeitungssystemen            |
| Reengineering                 | Etwas bestehendes neu implementieren (Ähnlich wie Rebuild)   |
| Reverse Engineering           | Das Ableiten oder Nachdokumentieren von Informationen über ein System |
| Rebuild                       | Etwas bestehendes neu bauen (Ähnlich wie Reengineering)      |
| Rehosting / Lift & Shift      | Migration der Kopie einer Anwendung (Oft in die Cloud)       |
| Retire                        | Ersetzen einer bestehenden Anwendung durch eine Cloud-native Applikation |
| Repurchase                    | Wechsel zu einem anderen Produkt, z. B. SaaS                 |
| Retain Software               | Kritische Anwendungen behalten und Migration auf später schieben |
| SaaS                          | Software-as-a-Service                                        |
| DEV (development environment) | Entwicklungsumgebung                                         |
| STAGE (staging environment)   | Testumgebung                                                 |
| LIVE (live environment)       | Im Deutsch auch oft "scharfe" oder Produktion-Umgebung oder  genannt |
| Integration environment       | Eine Umgebung, in der verschiedene Komponenten oder Module eines Systems integriert und getestet werden, um sicherzustellen, dass sie ordnungsgemäß zusammenarbeiten. |
| QA (Quality Assurance)        | Eine Umgebung, die speziell für umfassende Qualitätsprüfungen und Tests vor der Bereitstellung in der Produktionsumgebung vorgesehen ist. |
| Pre-Production                | Eine Umgebung, die der Produktionsumgebung ähnelt, aber für letzte Tests, Genehmigungen und möglicherweise auch für Schulungen oder Demonstrationszwecke vor der eigentlichen Bereitstellung verwendet wird. |
| UAT (User Acceptance Testing) | Eine Umgebung, in der Endbenutzer die Software testen und Feedback geben, um sicherzustellen, dass sie ihren Anforderungen und Erwartungen entspricht, bevor sie in Produktion geht. |
| Sandbox                       | Eine isolierte Umgebung, die zum Experimentieren, Testen neuer Funktionen oder zum Entwickeln von Prototypen verwendet wird, ohne die Produktionsumgebung zu beeinträchtigen. |
| Refactoring                   | Neuaufbau einer Anwendung                                    |
| Platform change               | Migration in eine neue Umgebung                              |
| DevOps                        | DevOps ist eine Kombination aus „Development" und „Operations", steht allerdings für eine ganze Reihe von Ideen und Praktiken, die über diese beiden Begriffe, egal ob einzeln oder zusammen, weit hinausgehen. |
| Legacy App / System           | Abzulösende Anwendung / System                               |
| Interface                     | Schnittstelle                                                |
| Live-Migration                | Umzug einer virtuellen Maschine (VM) im laufenden Betrieb    |
| Database First                | Hierbei wird als erstes das Datenbanksystem auf ein modernes System migriert |
| Database Last                 | Hierbei wird als letztes das Datenbanksystem auf ein modernes System migriert |
| Native App                    | Eine Anwendung, welche für eine bestimmte Geräteplattform geschrieben wurde |
| Web App                       | Eine Anwendung welche im Browser ausg                        |
| Risikoanalyse                 | Bewertung potenzieller Risiken und Herausforderungen während der Migration. |
| Kompatibilitätstests          | Überprüfung, ob die migrierte Software mit den neuen Umgebungen kompatibel ist. |
| Rollback-Plan                 | Plan zur Wiederherstellung des alten Systems im Falle von Problemen während der Migration. |
| Compliance                    | Einhaltung von gesetzlichen Vorschriften während der Migration. |
| Downtime                      | Die Zeit, in der das System für Wartungsarbeiten heruntergefahren wird |
| EOL                           | End of Life bedeutet, dass eine Software keine offiziellen Updates und/oder Support vom Hersteller erhält. |





