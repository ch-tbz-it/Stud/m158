# Übung - Migrationsansätze

<table>
  <tr>
    <th>Dauer</th>
    <td>120 Minuten</td>
  </tr>
  <tr>
    <th>Arbeit</th>
    <td>Einzelarbeit</td>
  </tr>
     <tr>
    <th>Abgabe</th>
    <td>Im eigenen Repository</td>
  </tr>
        <tr>
    <th>Typ</th>
    <td>Theorie / Diese Übung baut auf der vorgehenden Übung - Software-Migration auf</td>
  </tr>
</table>



## Beschreibung

Es gibt verschiedenste Software-Migrationsansätze, welche definieren, wie eine Software-Migration ablaufen kann.



## Migrationsansätze

- Cold Turkey bzw. Big Bang-Migrationsansatz
- Rehosting/ Lift-and-Shift
- Retire (Ersetzen der Software mit native Cloud-App)
- Refactor
- Rebuild / Reengineering
  - Chicken-Little-Migrationsansatz
  - Butterfly-Migrationsansatz
  - COREM-Migrationsansatz

- Repurchase
- Retain



## Auftrag

1. Nutzen Sie nun das [Skript Kapitel 3](https://tbzedu.sharepoint.com/:b:/s/campus/students/ESRntT35GnVKioMo4pdqwJIBZXK9XdUvT4xUVyPZVUvSDg?e=40bSgP), das [PDF Cloud Migration Checklist](https://gitlab.com/ch-tbz-it/Stud/m158/-/blob/main/04_Unterrichtsressourcen/Cloud-Migration-Checklist.pdf) Seite 7, das Internet und/oder ChatGPT, um die oben genannten Migrationsansätze kennen zu lernen
2. Entscheiden Sie sich für einen passenden Migrationsansatz für Ihre "imaginäre" Migration aus der Übung Software-Migrationsbeispiel
3. Erstellen Sie ein Ablauf-Diagramm für den groben Durchführungsplan aus etwa 10 bis 20 Schritten für Ihre Migration aus Auftrag 1. Nehmen Sie ein passenden Tool für die visuelle Darstellung. z.B. [https://draw.io]() und erstellen Sie eine Präsentation daraus
4. Speichern Sie die Datei "Übung - Migrationsansätze Vorname Nachname.md" im Ordner "Abgaben" in Ihrem Repository für das M158
5. Pushen Sie das Ergebnis mit dem "Comit Message" Übung - Migrationsansätze"
