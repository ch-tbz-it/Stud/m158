# Übung - Software-Migrationsbeispiel

<table>
  <tr>
    <th>Dauer</th>
    <td>20 Minuten</td>
  </tr>
  <tr>
    <th>Arbeit</th>
    <td>Einzelarbeit</td>
  </tr>
  <tr>
    <th>Abgabe</th>
    <td>Im eigenen Repository</td>
  </tr>
    <tr>
    <th>Typ</th>
    <td>Theorie</td>
  </tr>

</table>


## Beschreibung

In dieser Übung sollen Sie ein Beispiel für eine Software-Migration im Detail beschreiben. Überlegen Sie sich zuerst ein Beispiel für eine "imaginäre" Software oder Service-Migration und beantworten Sie anschliessend die folgende Fragen. 

*Am besten nehmen Sie nicht etwas zu einfaches, wie z.B. "Einzelplatz-Migration von MS Office" oder ähnlich.*

### Liste mit Ideen

- Betriebssystemwechsel in einer Umgebung mit mehreren Clients
- Umzug eines Webauftrittes von einem Hosting zum anderen
- Ersetzen des Webauftrittes auf demselben Hosting
- Server-Hardwaremigration ohne Softwareunterbruch
- Migration eines klassischen Serverdienst (DHCP/File/Print)
- Portierung einer lokalen Anwendung in die Cloud
- Eigene Ideen

### Fragen

*Beim beantworten der Fragen dürfen Sie natürlich kreativ sein, es soll aber auch so realistisch wie möglich sein.*

- Wie ist der Titel Ihrer Migration?
- Ziel und Zweck der Migration?
- Umfang: Welche Software oder welcher Service wird migriert?
- Hat die Software technische oder produktive Abhängigkeiten? Wenn ja, welche?
- Darf es eine "Downtime" der Software geben? Wenn ja, wie lange maximal?
- Welche Kosten entstehen bei einer längeren Downtown als erwartet?
- Wie ist die Benutzerakzeptanz?
- Brauchen es Schulung und Training?
- Wie sind die Testverfahren?
- Wie wird das Wissen über die alte und neue Software dokumentiert und archiviert?
-  Werden gesetzliche Bestimmungen und Vorschriften bei der Migration berücksichtigt?
- Wie werden Schnittstellen zu anderen Systemen und Anwendungen verwaltet?
- Wie werden Backup- und Wiederherstellungsverfahren während der Migration sichergestellt?
- Welcher Zeitrahmen wird für die Migration festgelegt und wie wird er überwacht?
- Wie wird die Kommunikation über die Migration intern und extern gehandhabt?
-  Welche Kosten sind mit der Migration verbunden und wie wird das Budget verwaltet?
- Was ist der Plan B, falls die Migration fehlschlägt oder Probleme auftreten?
- Wie wird der Erfolg der Migration gemessen und bewertet?



## Auftrag

Erstellen Sie eine neue Datei im Ordner "Abgaben" in Ihrem Repository für das M158 mit dem Namen **"Übung - Software-Migrationsbeispiel.md"**

1. In dieser Datei erstellen Sie einen Titel mit dem Namen Ihrer Software-Migration
2. Beantworten Sie die oben gestellten Fragen in dieser Datei
3. Pushen Sie das Ergebnis mit der "Comit Message" Übung - Software-Migrationsbeispiel"

