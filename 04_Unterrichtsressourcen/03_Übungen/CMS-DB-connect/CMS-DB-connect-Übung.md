# Übungen CMS-DB Connection

Verstehen Sie, wie und wann CMS-Applikationen eine Verbindung zu Datenbanken herstellen und wie der Ablauf vom Aufruf einer Webanwendung bis zur Datenbankabfrage ist.



## Übung 1

![Beispiel Super-CMS](https://gitlab.com/ch-tbz-it/Stud/m158/-/raw/main/04_Unterrichtsressourcen/03_%C3%9Cbungen/CMS-DB-connect/Beispiel%20Super-CMS-blank.png?ref_type=heads)

> Erklären Sie in 9 Schritten was passiert, wenn eine Benutzerin oder ein Benutzer das CMS https://supercms.ch im Browser öffnet (ohne DNS). 
>
> Versuchen Sie pro Schritt eine Verbindung in der Grafik zu machen.



## Übung 2

Überlegen Sie wo die folgenden Technologien/Protokolle in der Grafik im Einsatz sind

- https
- mysqli
- ext4

und tragen Sie dies bei den passenden Verbindungen ein.



## Übung 3

Passen Sie die DB-Config Datei auf die Umgebung aus dem Bild an. Speichern Sie die angepasste Config-Datei ins Repository