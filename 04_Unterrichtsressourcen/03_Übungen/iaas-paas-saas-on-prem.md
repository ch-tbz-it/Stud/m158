# Iaas Paas Saas On-Site/On-Premises

**Aufgabenstellung:**

Analysieren Sie die beiden Diagramme zu **IaaS, PaaS und SaaS** und vergleichen Sie deren Inhalte. Achten Sie dabei auf Unterschiede in der Darstellung und Perspektive. Versuchen Sie, eine sinnvolle Interpretation der beiden Grafiken zu erarbeiten und Verknüpfungen zwischen ihnen herzustellen.

Beantworten Sie dabei folgende Fragen:

- Welche Gemeinsamkeiten und Unterschiede lassen sich erkennen?
- Welche unterschiedlichen Schwerpunkte setzen die Diagramme?
- Wie lassen sich die Inhalte beider Grafiken sinnvoll miteinander in Verbindung bringen?

Fassen Sie Ihre Erkenntnisse in einer kurzen Analyse zusammen.



**Abgabe:**

- Halten Sie Ihre Ergebnisse in einem **übersichtlich formatierten Dokument** fest.
- Verwenden Sie **Überschriften, Absätze und ggf. Tabellen oder Aufzählungen**, um Ihre Analyse strukturiert darzustellen.
- Falls sinnvoll, ergänzen Sie eigene Skizzen oder Erklärungen zur Verdeutlichung.

Speichern Sie Ihr Dokument als **Markdown in Ihrem Repo und teilen Sie den Link im Team**. 



[Grafik 1](https://gitlab.com/ch-tbz-it/Stud/m158/-/blob/main/04_Unterrichtsressourcen/03_%C3%9Cbungen/IaasPaasSaas/iaas-paas-saas-diagram.png?ref_type=heads)

[Grafik 2](https://gitlab.com/ch-tbz-it/Stud/m158/-/blob/main/04_Unterrichtsressourcen/03_%C3%9Cbungen/IaasPaasSaas/shared-responsibility.svg?ref_type=heads)

