# Factsheet zur Software-Migration zwischen Linux und Windows

## Applikationen welche "grundsätzlich" auf Linux und Windows betrieben werden können

- Webapplikationen
- JAVA-Applikationen
- Python-Applikationen
- JavaScript/Node.js-Anwendungen
- C/C++-Anwendungen
- Mono/.NET Core-Anwendungen
- Containerisierte Anwendungen
- Plattformübergreifende GUI-Frameworks

Obwohl diese Programmiersprachen Plattform unabhängig sind können nicht dieselben Setup-Dateien zum installieren der Software verwendet werden. Nachfolgend erfahren Sie die Unterschiede der beiden Betriebssysteme Linux und Windows im Bezug 

## Dateirechte und Berechtigungen

- Linux verwendet standardmässig das POSIX-Rechtesystem
- Windows verwendet ACLs (Access Control Lists) und besitzt ein anderes Berechtigungssystem, das nicht direkt mit dem von Linux vergleichbar ist.



## Pfadunterschiede zwischen Linux und Windows

- Windows verwendet den Backslash (`\`) und Linux den Slash (`/`)
- Windows hat Laufwerksbuchstaben, und Linux verwendet ein einheitliches hierarchisches Dateisystem, das mit dem Root-Verzeichnis (`/`) beginnt.
- Linux ist "case sensitive"
- Windows ist nicht "case sensitive"
- Bei Windows hat jedes Laufwerk hat sein eigenes Root-Verzeichnis (z.B. `C:\`, `D:\`).
- Linux: Dateien und Verzeichnisse werden durch einen Punkt (.) vor dem Namen versteckt.
- Windows: Dateien und Verzeichnisse werden über ein Dateiattribut versteckt
- Grundsätzlich hat Windows und Linux andere Ordner- und Datei-Attribute.
- Linux unterstützt viel mehr Zeichen in der Pfadlänge. Traditionell hat Windows eine maximale Pfadlänge von 260 Zeichen und Linux 4096 Zeichen

## System- und Konfigurationsdateien

- Linux verwendet textbasierte Konfigurationsdateien (z.B. /etc/passwd für Benutzerinformationen).
- Windows verwendet die Registry sowie INI-Dateien und andere proprietäre Formate für Systemkonfigurationen.





