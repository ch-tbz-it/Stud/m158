# M158 - Knowledge Base

## AWS

[VPC-Umgebung in AWS einrichten](https://gitlab.com/ch-tbz-it/Stud/m158/-/tree/main/04_Unterrichtsressourcen/00_AWS/AWS-Netzwerk-Anleitung?ref_type=heads)



## OS

### Debian

- [Setup a LAMP (Debian 11) - Optimized method](https://dev.to/jakiboy/setup-a-lamp-debian-11-optimized-method-le1)

###  Ubuntu

- `lsb_release -a` (Ubuntu Version anzeigen)
- [Ubuntu Cheatsheet](https://cheatography.com/davechild/cheat-sheets/linux-command-line/)
- [APT sources.list (Jammy)](https://gist.github.com/hakerdefo/9c99e140f543b5089e32176fe8721f5f)
- [Change the user's home directory](https://stackoverflow.com/questions/20797819/command-to-change-the-default-home-directory-of-a-user)
- [Configure Ubuntu Networking with Netplan](https://dev.to/joeneville_/configure-ubuntu-networking-with-netplan-1cbc)
  - https://wiki.ubuntuusers.de/Netplan/ (Um das Risiko syntaktischer Fehler beim Erstellen einer **yaml-Datei** möglichst gering zu halten, sollte man im Editor die Anzeige von Leerzeichen einschalten. Dies ist z.B. für [Nano](https://wiki.ubuntuusers.de/Nano/) mittels Tastenkombination Alt + P möglich.)
- [Invalid YAML: mapping values are not allowed in this context: network:](https://askubuntu.com/questions/1256246/invalid-yaml-mapping-values-are-not-allowed-)
- [Netplan Cheatsheet & Example Configurations](https://blog.programster.org/netplan-cheatsheet)
- [Dnsmasq - Lightweight Name Resolution For Your Home Lab](https://dev.to/joeneville_/dnsmasq-lightweight-name-resolution-for-your-lab-2gim)
- [Apache installieren](https://ubuntu.com/tutorials/install-and-configure-apache#1-overview)
- [PHP installieren](https://ubuntu.com/server/docs/how-to-install-and-configure-php)
- [Install MySQL-Server](https://ubuntu.com/server/docs/install-and-configure-a-mysql-server)



## Apache & Virtual Hosts

- [Apache installieren](https://ubuntu.com/tutorials/install-and-configure-apache#1-overview)

- `apachectl configtest` Apache Konfiguration übeprüfen

- [VirtualHost Examples](https://httpd.apache.org/docs/2.4/vhosts/examples.html)

  Creating virtual host configurations on your Apache server does not magically cause DNS entries to be created for those host names. You *must* have the names in DNS, resolving to your IP address, or nobody else will be able to see your web site. You can put entries in your `hosts` file for local testing, but that will work only from the machine with those `hosts` entries.

- [Default Page still accessible after a2dissite 000-default.conf](https://askubuntu.com/questions/846647/default-page-still-accessible-after-a2dissite-000-default-conf)
- [HTTP auf HTTPS Weiterleitung](https://httpd.apache.org/docs/current/rewrite/avoid.html#redirect) 
- [.htaccess (Allow Override)](https://httpd.apache.org/docs/2.4/howto/htaccess.html)
- [HTTP auf HTTPS Weiterleitung mittels .htaccess](https://www.freecodecamp.org/news/how-to-redirect-http-to-https-using-htaccess/)

## NGINX

- [Beginners Guide](https://nginx.org/en/docs/beginners_guide.html)

## DNS & SSL

- [Self-signed SSL](https://www.digitalocean.com/community/tutorials/how-to-create-a-self-signed-ssl-certificate-for-apache-in-ubuntu-20-04) 
- [Certbot - Let's Encrypt Zertifikat](https://www.techhut.tv/how-to-apache-webserver-ssl/) (Nur mit Public-Domäne)
- [OpenSSL CRT & KEY Creation](https://stackoverflow.com/questions/10175812/how-to-generate-a-self-signed-ssl-certificate-using-openssl) (Note: After 2015, certificates for internal names will no longer be trusted.)

## FTP

- [FTP-Server installieren](https://ubuntu.com/server/docs/set-up-an-ftp-server)
- [Change default ftp directory](https://askubuntu.com/questions/961231/how-to-change-default-ftp-directory)

## MYSQL

- [Install MySQL-Server](https://ubuntu.com/server/docs/install-and-configure-a-mysql-server)
- [PHPMyAdmin installieren](https://ubuntu.com/server/docs/how-to-install-and-configure-phpmyadmin)
- [Change MYSQL-Server bind](https://vexxhost.com/resources/tutorials/how-to-allow-remote-access-to-mysql/) (Externe Zugriff phpmyadmin)

## PHP

###  FastCGI Process Manager (FPM)

[All you need to know about FastCGI Process Manager (FPM)I](https://www.youtube.com/watch?v=hEXBgQ71rvE) 

### PHP-Einstellungen anpassen

Wenn Sie die Einstellungen über die `.htaccess` Datei vornehmen, beachten Sie die Einstellung (Allow Override) im Virtual Host. Siehe Apache weiter oben.

Edit .htaccess file

```
php_value upload_max_filesize 128M
php_value post_max_size 128M
php_value memory_limit 256M
php_value max_execution_time 300
php_value max_input_time 300
```

or

Edit wp-config.php file

```
@ini_set( 'upload_max_filesize' , '128M' );
@ini_set( 'post_max_size', '128M');
@ini_set( 'memory_limit', '256M' );
@ini_set( 'max_execution_time', '300' );
@ini_set( 'max_input_time', '300' );
```



## WordPress

### Schritte für die Migration

1. Backup via FTP herunterladen. Verbindungsdaten finden Sie hier
2. ZIP-Datei entpacken und alle Dateien (ausser das Datenbank-Backup *.sql) in Ihr DocumentRoot vom Virtual Host kopieren
3. Datenbank-Backup zurückladen
4. wp_config.php mit Ihren Verbindungsdaten für die WordPress-Datenbank anpassen
5. In der Tabelle VtgnJGv_options die SITE_URL und HOME Werte anpassen
6. MD5-Hash generieren und in der Tabelle anpassen VtgnJGv_users für User m158 ersetzen anschliessend login Test über https://IHRE-DOMAIN/wp-admin
7. Im Backend anmelden und [die durch das Theme statisch erzeugten CSS-Dateien löschen](https://gitlab.com/ch-tbz-it/Stud/m158/-/blob/main/04_Unterrichtsressourcen/04_Tutorials%20&%20Befehle/Statische-erzeugte-CSS-Dateien-l%C3%B6schen.png?ref_type=heads), damit diese auf Ihrer Umgebung automatisch neu erstellt werden.
8. Better Search Replace Plugin installieren und ganze Datenbank suchen nach "alte Domain" ersetzen mit "neue Domain"



## Tutorials

- [How to make an Apache Webserver with SSL](https://www.techhut.tv/how-to-apache-webserver-ssl/)

- [WordPress Installation unter Ubuntu](https://ubuntu.com/tutorials/install-and-configure-wordpress#1-overview)



