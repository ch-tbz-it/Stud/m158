# Linux Terminal-Befehle

## Ubuntu

### Benutzerverwaltung

- [Cheat-Sheet](https://linuxsimply.com/wp-content/uploads/2023/05/Linux-ubuntu-commands-cheat-sheet-by-linuxsimply.pdf) (PDF)
- [Cheat-Sheet](https://gist.github.com/miranda-zhang/c580cc45bed3317501a6071726d929c0) (Text)
- [Alle Benutzer anzeigen]()



## Terminal

In Dateien nach einem Text suchen z.B. `grep -i 'mod_cache' /etc/apache2/mods-available/*`

https://www.youtube.com/watch?v=Tc_jntovCM0



## Apache

- Überprüfung mit welchem User Apache ausgeführt wird: `ps aux | grep apache2`
- Apache Log löschen: `sudo bash -c 'echo > /var/log/apache2/error.log'`
- Apache Log auslesen:  `sudo tail -f /var/log/apache2/error.log`
- Apache alle Module anzeigen:  `apachectl -M` 



