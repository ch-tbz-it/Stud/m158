# VPC-Umgebung in AWS einrichten


## Inhaltsverzeichnis

[TOC]

## 1 Einleitung

In dieser Anleitung wird genau beschrieben, wie eine einfache Netzwerkumgebung in AWS mithilfe von VPC, Subnetzen, Routingtabellen, Sicherheitsgruppen und einem Zugang zum Internet geschaffen wird. Die fertige Umgebung wird schlussendlich folgendermassen aussehen:

![Netzwerkplan](Ressourcen/Bilder/AWS-Netzwerk-Anleitung_Netzwerkplan.png)


## 2 Umsetzung

### 2.1 VPC

Als Erstes wird ein VPC (Virtual Private Cloud) erstellt. Dazu sucht man im Startmenü von AWS im Suchfeld oben nach „VPC“ und navigiert dann in das erscheinende VPC-Dashboard.

![VPC-Dashboard](Ressourcen/Bilder/VPC-Dashboard.png)

**Konfigurationen für das VPC-Setup:**

| Konfiguration           | Wert                          |
|-------------------------|-------------------------------|
| Zu erstellende Ressourcen | Nur VPC (der Einfachheit halber) |
| Namens-Tag               | demo_vpc                      |
| IPv4-CIDR-Block          | Manuelle IPv4 CIDR-Eingabe    |
| IPv4-CIDR                | 10.0.50.0/23                 |
| IPv6-CIDR-Block          | Kein IPv6 CIDR-Block          |
| Tenancy                  | Standard                     |

Nachdem alle diese Konfigurationen vorgenommen worden sind, kann man mit „VPC erstellen“ das virtuelle Netzwerk erstellen lassen. Zum jetzigen Zeitpunkt sieht die Umgebung folgendermassen aus:

![VPC](Ressourcen/Bilder/VPC.png)

### 2.2 Subnetze

Im zweiten Schritt werden die beiden Subnetze erstellt. Dies hätte man auch schon im Setup vom VPC machen können, einfacher ist es aber, diese Dinge getrennt voneinander aufzusetzen und dann erst miteinander zu verknüpfen.
Im VPC-Dashboard findet man den Reiter „Subnetze“. Diesen wählt man aus und klickt dann oben rechts auf die Option „Subnetz erstellen“.

![Subnetze](Ressourcen/Bilder/VPC-Dashboard-2.png)

Daraufhin öffnet sich erneut ein Setup, bei welchem die folgenden Konfigurationen eingetragen werden müssen:

**Konfigurationen für Subnetz 1:**

| Konfiguration           | Wert                     |
|-------------------------|--------------------------|
| VPC-ID                  | demo_vpc                 |
| Name                    | demo_subnet_01           |
| Verfügbare Zone          | us-east-1a               |
| IPv4-Subnetz-CIDR-Block | 10.0.50.0/24             |

**Konfigurationen für Subnetz 2:**

| Konfiguration           | Wert                     |
|-------------------------|--------------------------|
| Name                    | demo_subnet_02           |
| Verfügbare Zone          | us-east-1a               |
| IPv4-Subnetz-CIDR-Block | 10.0.51.0/24             |

Nun kann man das Setup beenden, indem man ganz unten auf die Option „Subnetz erstellen“ klickt. Die beiden Subnetze wurden somit bereitgestellt, was die aktuelle Umgebung folgendermassen aussehen lässt:

![VPC+Subnetz](Ressourcen/Bilder/VPC+Subnetze.png)

### 2.3 Internet-Gateway

Ein Internetgateway wird erstellt, um den gesamten unbekannten Netzwerk-Traffic ins Internet zu leiten.

Im VPC-Dashboard wählt man nun also den Reiter „Internet-Gateways“ und dann „Internet-Gateway“ erstellen.

![IGW](Ressourcen/Bilder/IGW.png)

Die einzige Konfiguration, welche im Setup des Internet-Gateways gemacht werden muss, ist der Name:

**Konfigurationen für das Internet-Gateway:**

| Konfiguration | Wert                |
|---------------|---------------------|
| Name          | demo_internetgateway |

Danach kann man bereits auf „Internet-Gateway erstellen“ klicken. Im Internet-Gateway Reiter wählt man nun den eben erstellten Internet-Gateway aus und wählt dann oben in den Aktionen „An VPC anfügen“.

![IGW-2](Ressourcen/Bilder/an%20VPC%20anfügen.png)

Als VPC-Option wählt man dann logischerweise das erstellte demo_vpc. Die aktuelle Umgebung sieht nun folgendermassen aus:

![VPC+Subnetz+IGW](Ressourcen/Bilder/VPC+Subnetze+IGW.png)


### 2.4 Routing-Tabelle

Wie im Bild oben zu erkennen ist, wurde der Internet-Gateway zwar erstellt, jedoch ist noch nirgendwo geregelt, dass der unbekannte Traffic auch tatsächlich an ihn geleitet werden soll. Dies lässt sich mit einer Routing-Tabelle einrichten.
Im VPC-Dashboard navigiert man zum Reiter „Routing-Tabellen“, und wählt dann „Routing-Tabelle erstellen“. 

![Routingtabellen](Ressourcen/Bilder/RoutingTable.png)

Im Setup konfiguriert man hier auch wieder nur den Namen (demo_routingtable) und das verknüpfte VPC (demo_vpc). Nach Beendigung des Setups wird einem direkt die eben erstelle Routing-Tabelle angezeigt. In diesem Dashboard wählt man nun unten bei den Routen „Routen bearbeiten“.

![routen bearbeiten](Ressourcen/Bilder/Routen%20bearbeiten.png)

Wie man erkennen kann, wurde automatisch bereits eine Route erstellt, diese kann man so lassen. Hier kann man nun eine neue Route hinzufügen und folgendermassen konfigurieren:

![routen bearbeiten-2](Ressourcen/Bilder/Routen%20bearbeiten-2.png)

Als Internet-Gateway wählt man hier den demo_internetgateway aus. 

Als nächstes muss man nun die beiden Subnetze mit der Routing-Tabelle verbinden. Dazu wählt man im Reiter „Routing-Tabelen“ die demo_routingtable aus und navigiert unter „Aktionen“ zu „Subnetzzuordnungen bearbeiten“.

![Subnetzzuordnung](Ressourcen/Bilder/Subnetzzuordnung.png)

Hier wählt man nun die beiden erstellten Subnetze aus und speichert die neuen Verknüpfungen. Mit diesen Schritten sieht die aktuelle Umgebung folgendermassen aus und alles, was bleibt, sind die EC2-Instanzen und die dazugehörenden Sicherheitsgruppen:

![VPC+Subnetz+IGW+RT](Ressourcen/Bilder/VPC+Subnetze+IGW+RT.png)

### 2.5 Sicherheitsgruppen

Im Vierten Schritt werden nun die Sicherheitsgruppen erstellt, welche als eine Art Firewall dienen und den Traffic von innen nach draussen und von draussen nach innen regeln. 
Für diese Sicherheitsgruppen sucht man oben in der Suchleiste nach dem Service „EC2“

![EC2-Dashboard](Ressourcen/Bilder/EC2-Dashboard.png)

Im EC2-Dashboard scrollt man nun runter bis zum Reiter „Netzwerk & Sicherheit“. Dort wählt man „Sicherheitsgruppen“ und im erscheinenden Dashboard dann „Sicherheitsgruppe erstellen“.

![Sicherheitsgruppe](Ressourcen/Bilder/Sicherheitsgruppe.png)

**Konfigurationen für die Sicherheitsgruppe:**

| Konfiguration | Wert                  |
|---------------|-----------------------|
| Name          | demo_securitygroup     |
| Beschreibung  | demo_securitygroup     |
| VPC           | demo_vpc              |

Die restlichen Konfigurationen können erst vorgenommen werden, wenn die Sicherheitsgruppe erstellt worden ist. Somit beendet man an dieser Stelle das Setup und erstellt somit die Sicherheitsgruppe.
In der Liste mit allen Sicherheitsgruppen klickt man nun auf die ID der demo_securitygroup. Dies öffnet die Verwaltung dieser Securitygroup. 

![Sicherheitsgruppe-2](Ressourcen/Bilder/Sicherheitsgruppe-2.png)

Ganz unten in diesem Dashboard unter „Regeln für eingehenden Datenverkehr“ wählt man nun „Regeln für eingehenden Datenverkehr bearbeiten“. Da noch keine Regeln vorhanden sind, muss man eine neue hinzufügen.

| Konfiguration Regel 1 | Wert                          |
|-----------------------|-------------------------------|
| Typ                   | Gesamter Datenverkehr         |
| Quelle                | Benutzerdefiniert --> demo_securitygroup |
| Beschreibung          | -                             |

Diese Regel erlaubt sämtlichen Datenverkehr von allen Protokollen, jedoch nur, wenn sich das Gerät, welches diesen Traffic verursacht, bzw. empfängt, in der Sicherheitsgruppe **demo_securitygroup** befindet.

Bei der zweiten und dritten Regel wird nun noch der RDP- bzw. der SSH-Zugang von **außen** ermöglicht.

| Konfiguration Regel 2 | Wert                          |
|-----------------------|-------------------------------|
| Typ                   | RDP                           |
| Quelle                | Anywhere-IPv4 --> 0.0.0.0/0   |
| Beschreibung          | -                             |

| Konfiguration Regel 3 | Wert                          |
|-----------------------|-------------------------------|
| Typ                   | SSH                           |
| Quelle                | Anywhere-IPv4 --> 0.0.0.0/0   |
| Beschreibung          | -                             |

Zum Schluss kann man diese Regeln noch speichern und das Setup schliessen. Die aktuelle Umgebung sieht so aus:

![VPC+Subnetz+IGW+RT+SG](Ressourcen/Bilder/VPC+Subnetze+IGW+RT+SG.png)

Der Netzwerk-Teil wäre somit auch bereits abgeschlossen. Alles, was jetzt noch zu tun ist, um eine funktionierende Umgebung zu schaffen, wäre es, EC2-Instanzen (VMs) zu deployen.

### 2.6 EC2

In diesem letzten Schritt werden zwei Ubuntu Instanzen erstellt, welche lediglich dazu dienen, die bis jetzt aufgesetzte Netzwerk-Umgebung zu testen. 
Im EC2-Dashboard navigiert man zum Reiter „Instances“ und von dort aus dann zu „Instances starten“

![Instances](Ressourcen/Bilder/Instanz%20starten.png)

Die Konfigurationen für diese Instanzen lauten wie folgt:

**Konfigurationen für Instance 01:**

| Konfiguration                 | Wert                     |
|-------------------------------|--------------------------|
| Name                          | demo_instance-01         |
| Amazon Machine Image (AMI)     | Ubuntu Server 24.04 LTS  |
| Instance-Typ                   | t2.micro                 |
| Schlüsselpaar                  | Neues erstellen (demo_keypair) |
| Netzwerkeinstellungen          | demo_vpc                 |
| Subnetz                        | demo_subnet_01           |
| Öffentliche IP automatisch zuweisen | Aktivieren           |
| Firewall (Sicherheitsgruppen)  | demo_securitygroup       |

**Konfigurationen für Instance 02:**

| Konfiguration                 | Wert                     |
|-------------------------------|--------------------------|
| Name                          | demo_instance-02         |
| Amazon Machine Image (AMI)     | Ubuntu Server 24.04 LTS  |
| Instance-Typ                   | t2.micro                 |
| Schlüsselpaar                  | Neues erstellen (demo_keypair) |
| Netzwerkeinstellungen          | demo_vpc                 |
| Subnetz                        | demo_subnet_02           |
| Öffentliche IP automatisch zuweisen | Aktivieren           |
| Firewall (Sicherheitsgruppen)  | demo_securitygroup       |

Die beiden Instances sind nun erstellt, was die Umgebung somit komplett macht. Im nächsten und letzten Kapitel wird die Umgebung noch mit den eben erstellten VMs getestet.

## 3  Testing

### 3.1 Vorbereitung

Für das Testing verbindet man sich über SSH mit den beiden Ubuntu-Instanzen. Dies geschieht mit folgendem CMD-Befehl:

```bash
ssh -i "Pfad zum gespeicherten demo_keypair" ubuntu@öffentliche_IP_der_Instanz
```

![ssh-connect](Ressourcen/Bilder/ssh-connect.png)

Mit folgendem Befehl installiert man sich nun die Netzwerk-Tools, um die IP-Konfiguration anzeigen zu lassen:

```bash
sudo apt install net-tools
```

### 3.2 Testprotokkolle

| Test-Nr. | Was wurde getestet                         | Wie wurde es getestet | Erwartetes Ergebnis                                |
|----------|--------------------------------------------|-----------------------|----------------------------------------------------|
| **1**        | Hat die IP-Zuweisung geklappt?             | `ifconfig`            | Beide Instanzen haben eine IP von ihrem Subnetz    |
| **2**        | Funktioniert der Traffic zwischen den Instanzen | `ping`             | Beide Instanzen können sich gegenseitig pingen     |
| **3**        | Funktioniert der Zugang zum Internet       | `ping 8.8.8.8`        | Beide Instanzen können erfolgreich den Google-DNS anpingen |


| Test-Nr. | Bild 1 | Bild 2 |
| ----------- | ----------- | ----------- |
| **1** | ![testing-1](Ressourcen/Bilder/testing-1.png) | ![testing-2](Ressourcen/Bilder/testing-2.png) |
| **2** | ![testing-3](Ressourcen/Bilder/testing-3.png) | ![testing-4](Ressourcen/Bilder/testing-4.png) |
| **3** | ![testing-5](Ressourcen/Bilder/testing-5.png) | ![testing-6](Ressourcen/Bilder/testing-6.png) |