# Proxmox

```
***Hinweis:***

Wenn Sie die Proxmox-Umgebung der Schule nutzen, finden Sie hier die nötigen Informationen. 
```

https://tbz.hopto.org:8006

**Das Proxmox-Login erhaltet Ihr im Teams Kanal**



## Netzwerk 

- Alle virtuellen Maschinen haben als Netzwerkschnittstelle "vmbr2" per Standard
- Setzen Sie für sämtliche LAN-Schnittstellen(Alle Schnittstellen für Ihre virtuelle Umgebung) einen individuellen VLAN-Tag. 
  (Am besten verwenden Sie Ihre Pool-ID)
- Die WAN-Schnittstelle auf dem Router ändern Sie auf "vmbr0" ohne VLAN-Tag, anschliessend können Sie eine IP über DHCP beziehen



### Snapshots

Bitte fahren Sie Ihre VM vor einem Snapshot herunter, da LIVE-Snapshots (ohne Shotdown) wesentlich mehr Ressourcen benötigen. Sie können jederzeit Snapshots erstellten.

### NOVNC

Standardmässig arbeitet Proxmox mit noVNC. Leider können Sie über die noVNC Console nicht copy/paste ausführen. 

Als Alternative können Sie [Tailscale](https://gitlab.com/ch-tbz-it/Stud/m158/-/blob/main/04_Unterrichtsressourcen/00_Proxmox/Tailscale/Guide.md) auf Ihrem Router und ein VPN auf Ihre Umgebung einrichten.



### Instruktionen (Video): 

https://vimeo.com/834389792/9e43089766

### OS auf einer VM neu aufsetzen(Video mit Untertitel):

[https://vimeo.com/943140968/0d4f8aaa8b]()



### VM-Einstellungen

[VM-Benennen](https://gitlab.com/ch-tbz-it/Stud/m158/-/blob/main/04_Unterrichtsressourcen/02_Bilder/VM-benennen.png)



#### Harddisk vergrössern

Nachdem Sie von der Lehrperson mehr Diskspace erhalten haben, können Sie mit der folgenden Software die Partition erweitern

https://www.paragon-software.com/de/free/pm-express/



### Wichtiges:

[Statische IP setzen mit YAML (Achtung Syntax)](https://askubuntu.com/questions/1256246/invalid-yaml-mapping-values-are-not-allowed-in-this-context-network)





