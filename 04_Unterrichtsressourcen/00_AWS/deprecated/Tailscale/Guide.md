# Tailscale Installationsanleitung

- [Tailscale Installationsanleitung](#tailscale-installationsanleitung)
  - [Vorbreitung](#vorbreitung)
  - [Client Verbinden](#client-verbinden)
  - [PfSense Konfiguration](#pfsense-konfiguration)
  - [Abschliessende einstellungen](#abschliessende-einstellungen)
    - [RDP Auf den VM's einschalten](#rdp-auf-den-vms-einschalten)



## Wichtige Hinweise

- Verbinden Sie Tailscale nicht mit Ihrem TBZ Account. Es wird keine Admin Console angezeigt und Tailscale kann nicht korrekt eingerichtet und verwendet weren

- Kopieren Sie die Auth  Key mit https://copypaste.me/ auf die pfSense

- Probleme mit pfsense Update:
  https://docs.netgate.com/pfsense/en/latest/troubleshooting/upgrades.html

- Troubleshooting "tailscale is not online. refresh or check the tailscale status page" on the Tailscale Plugin on PfSense.

  `Go to Pfsense Server` (Console)
  `8 (Open the shell)` 
  `try "tailscale ip" & "tailscale status"`
  `do "tailscale login"`
  `Authenticate and problem solved...`

  

## Vorbreitung

1. Snapshot von Router machen(falls es zu problem kommen sollte kann dieser Restored werden)
2. Tailscale Accoutn erstellen (https://login.tailscale.com/start) --> Am einfaschten einfach mit Google, Microsoft oder GitHub Account registrieren.


## Client Verbinden
1. Tailscale herunterladen (https://tailscale.com/download/)
2. Tailscale Starten
3. Anmeldung (in icontray Rechtscklick auf Tailscale Symbol)
![Alt text](images/ClientLogin.png)
4. Status im WebUI Überprüfen
![Alt text](images/ClientInWebui.png)



## PfSense Konfiguration
1. PFSense Webui öffnen
2. Package Manager Öffnen
![Alt text](images/PackageManager.png)

3. Tailscale Package Installieren(auf Install klicken)
![Alt text](images/DownloadTailscale.png)

4. Tailscale In PFSense öffnen
![Alt text](images/TailscaleVPN2.png)
![Alt text](images/TailscaleVPN.png)

5. In Tailscale Webui Auth Key generieren
![Alt text](images/GenerateKey.png)

6. Auth Key in PFSense Eingeben
![Alt text](images/AddKey.png)

7. Routes Hinzufügen und Tailscale Enablen 
![Alt text](images/AddRoutes.png)
(Routes sind die Subnetze welche Exposed Werden Sollten(diese sind dann für die Clients welche mit Tailscale verbunden werden verfügbar))
![Alt text](images/AddRoutesPFSense.png)

8. Tailscale Starten



## Abschliessende einstellungen
1. Im Webui pfsense Auswählen und Edit route settings auswählen.
![Alt text](images/2ClientsInWebui.png)
2. Routen einschalten
![Alt text](images/EnableRoutes.png)

### RDP Auf den VM's einschalten

![Alt text](images/RDP.png)









