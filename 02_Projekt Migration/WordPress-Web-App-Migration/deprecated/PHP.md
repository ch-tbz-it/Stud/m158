# Auftrag 6 - PHP

<table>
  <tr>
    <th>Dauer</th>
    <td>90 Minuten </td>
  </tr>
  <tr>
    <th>Arbeit</th>
    <td>Einzelarbeit</td>
  </tr>
     <tr>
    <th>Abgabe</th>
    <td>Im eigenen Repository (Siehe Abschnitt Resultat pro / Aufgabe separat geregelt)</td>
  </tr>
        <tr>
    <th>Typ</th>
    <td>Praktisch auf Ubuntu Server</td>
  </tr>
      </tr>
        <tr>
    <th>Erforderlich</th>
    <td>6a-c Ja, 6d Nein</td>
  </tr>
      </tr>
        <tr>
    <th>Punkte</th>
    <td>4 x 1 Punkt</td>
  </tr>
</table>




## Aufgabe 6a: PHP

 

Installieren Sie PHP für Apache2 gemäss Tutorial unter Hinweise. Erstellen Sie dann eine index.php Datei in Ihrem «virtual Host», mit der folgenden ECHO-Anweisung:

`echo «<h1>PHP funktioniert</h1>»;` 
       `(Diesen PHP-Befehl müssen Sie selber vervollständigen)`

Nun sollt diese PHP-Datei im Webserver bzw. im Virtual Host platziert werden und mit einem Zugriff über Ihren Hostnamen angezeigt werden.

 

**Hinweise**:

- https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-ubuntu-18-04-de (Schritt 3 — Installation von PHP)

**Resultat:**

- Inhalt der index.php im Portfolio abspeichern 
- Resultat wie die index.php im Browser angezeigt wird (Wenn PHP richtig geladen wird, wird nur der Titel «PHP funktioniert» ausgegeben

 

## Aufgabe 6b: Die Funktion phpinfo(); ausgeben

 

Erstellen eine neue Datei mit dem Namen phpinfo.php im selben Ordner, wo Sie vorher die Datei index.php erstellt haben

Fügen Sie die php Funktion phpinfo() in die neue Datei phpinfo.php

Führen Sie nun den Code aus der Datei phpinfo.php mit dem Befehl «include» über die index.php aus

**Hinweise:**

- https://www.w3schools.com/php/php_includes.asp
- https://www.php.net/manual/de/function.phpinfo.php

 

**Resultat:**

- Erstellen Sie einen Printscreen vom Abschnitt «Apache Environment» aus der phpinfo() Ausgabe




## Aufgabe 6c: PHP-Umgebungsvariablen anpassen 

 

- Erhöhen Sie den Wert «memory_limit» auf 256Megabyte
- Erhöhen Sie den Wert «max_execution_time» auf 90 Sekunden
- Erhöhen Sie den Wert «upload_max_filesize» auf 256Megabyt
- Überprüfen Sie das Ganze mit der Funktion phpinfo()

**Hinweise**:

- https://www.php.net/manual/de/configuration.file.php




**Resultat:**

- Erstellen Sie einen Printscreen von dem angepassten Wert aus der phpinfo()




## Aufgabe 6d: phpinfo() in Datei speichern

 

Ausgabe von phpinfo() in Datei speichern

**Hinweise**:

- https://gist.github.com/brianjking/2f3ea0f94d0348d38c6c

- «sudo php phpscript» im Terminal

**Resultat:**

- Erstellen Sie ein Bildschirm Video als Beweis

 

 
