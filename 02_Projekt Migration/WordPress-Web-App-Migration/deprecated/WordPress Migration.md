# Auftrag 10 - WordPress Migration

<table>
  <tr>
    <th>Dauer</th>
    <td>60 Minuten </td>
  </tr>
  <tr>
    <th>Arbeit</th>
    <td>Einzelarbeit</td>
  </tr>
     <tr>
    <th>Abgabe</th>
    <td>Im eigenen Repository (Siehe Abschnitt Resultat pro / Aufgabe separat geregelt)</td>
  </tr>
        <tr>
    <th>Typ</th>
    <td>Praktisch auf Ubuntu Server</td>
  </tr>
      </tr>
        <tr>
    <th>Erforderlich</th>
    <td>Ja</td>
  </tr>
      </tr>
        <tr>
    <th>Punkte</th>
    <td>3 x 1 Punkt</td>
  </tr>
</table>

## Aufgabe 10a: WordPress Datei-Restore 

 

In der Aufgabe 7 müssen Sie ein WordPress-Backup auf Ihrem virtualen Host wiederherstellen. Laden Sie als erstes das Backup als Zip herunter. Ein Voll-Backup enthält immer die CMS-Dateien als viele einzelne Dateien (In diesem Fall WordPress) und ein SQL-Backup mit dem dynamischen Content des CMS.

***Achtung: Falls Sie Ihre aktuelle WordPress-Installation behalten möchten (aus privaten Gründen, für das Modul158 ist diese nicht mehr relevant) laden Sie das Backup, welches Sie mit BackWPUP bei Aufgabe 6 erstellt haben via FTP herunter.***

 Download Backup: [Restore-WP-M158.zip](https://tbzedu-my.sharepoint.com/:u:/g/personal/jonas_aeschlimann_tbz_ch/EXLIVJd3s5ZNn8Z_QFx-oDkB5OoYwoYWa-BdJ2ScSYEPzg?e=naYwF4)

**Hinweise:**

- Löschen Sie alle WordPress-Dateien und machen Sie einen Restore sämtlicher Dateien aus dem Restore-WP-M158.zip mittels FTP.

**Resultat:**

- Resultat erfolgt bei Aufgabe 10c

 

## Aufgabe 10b - WordPress Backup Restore

 

**Hinweise:**

- Machen Sie auf der aktuellen Maria-DB Datenbank von Ihrem aktuellen WordPress eine Backup-Wiederherstellung mit phpmyadmin von der Datei wordpress.sql aus dem Backup Zip-File, welches Sie bereits heruntergeladen haben
- Gehen Sie nach dem erfolgreichen Restore auf die Tabelle wp_options und passen Sie die beiden Felder mit dem option_name “siteurl” und “home” mit dem Namen ihres virtuellen Host an. *(Aktueller Wert aus dem Backup: http://wordpress.m158 )*
   Achtung: Falls Ihr virtual Host mit HTTPS läuft, müssen Sie den Protokoll-präfix in der URL dementsprechend anpassen
- https://kinsta.com/knowledgebase/how-to-restore-mysql-database-using-phpmyadmin/



**Resultat:**

- Resultat erfolgt bei Aufgabe 10c




## Aufgabe 10c:  WordPress Config anpassen

 

**Hinweise**:

- Durch den Restore der WordPress-Dateien einer “fremden” WordPress-Umgebung, sind die Datenbankanmeldeinformationen nicht mehr korrekt
- Passen Sie die Angaben Ihrer Umgebung an



**Resultat:**

- Öffnen Sie Ihren virtuellen Host im Browser. Wenn der Restore erfolgreich war, sollten Sie nun eine Erfolgsmeldung sehen. Machen Sie einen Printscreen inklusive URL und speichern Sie diesen in Ihrem Portfolio ab

 

 
