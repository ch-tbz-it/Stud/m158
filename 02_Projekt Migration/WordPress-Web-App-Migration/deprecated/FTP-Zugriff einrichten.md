# Auftrag 5 - FTP Zugriff einrichten

<table>
  <tr>
    <th>Dauer</th>
    <td>90 Minuten </td>
  </tr>
  <tr>
    <th>Arbeit</th>
    <td>Einzelarbeit</td>
  </tr>
     <tr>
    <th>Abgabe</th>
    <td>Im eigenen Repository (Siehe Abschnitt Resultat pro / Aufgabe separat geregelt)</td>
  </tr>
        <tr>
    <th>Typ</th>
    <td>Praktisch auf Ubuntu Server</td>
  </tr>
      </tr>
        <tr>
    <th>Erforderlich</th>
    <td>Nein</td>
  </tr>
      </tr>
        <tr>
    <th>Punkte</th>
    <td>3 x 1 Punkt</td>
  </tr>
</table>



## Aufgabe 5a: FTP-Zugriff einrichten für User ubuntu

 

Installieren Sie den FTP-Dienst vsftpd mit folgenden Befehlen

- sudo apt update
- sudo apt install vsftpd
- sudo cp /etc/vsftpd.conf /etc/vsftpd.conf.orig (Kopie der Config-Datei erstellen)
- sudo ufw allow 20,21,990/tcp

Richten Sie einen neuen Benutzer ein, welchen Sie dann für die FTP Verbindung-verwenden

Erstellen Sie im Homeverzeichnis des neuen User einen Ordner ftp (Ordner für FTP Verbindung)

Anpassungen an der FTP Konfiguration (/etc/vsftpd.conf)

- write_enable=YES
- allow_writeable_chroot=YES

 

 **Hinweise:**

- Installieren Sie den FTP-Client FileZilla auf Ihrem lokalen Laptop

 

**Resultat:**

- Verbinden Sie mit dem neuen User auf den FTP-Server mit FileZilla und erstellen Sie einen Printscreen vom Verbindungsprotokollierung von FileZilla

 



 

## Aufgabe 5b: WordPress Document Root mounten

 

Damit der User «ubuntu» die Daten von WordPress später am richtigen Ort hochladen kann, müssen Sie das Verzeichnis von Ihrem virtualen Apache Host in den «ftp» Order im Homeverzeichnis mounten.

Ein «mount –bind» Befehl ist nur solange vorhanden, bis das OS neu gestartet wird. 

Machen Sie zuerst mit dem «mount –bind» den Test und richten Sie anschliessend mit der Anleitung unter Hinweis einen dauerhaften mount ein.

 

 

 

**Hinweise**:

- Erstellen Sie den Bind mit dem Befehl (Achtung: Pfad anpassen)
   mount --bind /var/www/«virtualHost» /home/ubuntu/ftp
- https://serverfault.com/questions/141504/mount-bind-persistence-over-reboot

 

 **Resultat:**

- Erstellen Sie einen Printscreen von FileZilla, wo man die gemountete «index.html» Datei, aus dem Virtual Host, im Home Verzeichnis vom User «ubuntu» sieht



## Aufgabe 5c: Benutzer zu www-data Gruppe hinzufügen und Berechtigungen anpassen

 

- Fügen Sie den Benutzer «ubuntu» in die Gruppe «www-data»
- Schreibrechte für die Gruppe «www-data» auf dem Virtual Host Verzeichnis geben

 

**Hinweise:**

https://www.howtogeek.com/50787/add-a-user-to-a-group-or-second-group-on-linux/

Beispiel:

sudo chmod o=r wordpress
 o=other
 r=read

 

**Resultat:**

- Machen Sie einen Printscreen von den Berechtigungen des Document Root ihres Virtual Hosts
