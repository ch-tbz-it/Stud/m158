# Auftrag 8 - WordPress

<table>
  <tr>
    <th>Dauer</th>
    <td>90 Minuten </td>
  </tr>
  <tr>
    <th>Arbeit</th>
    <td>Einzelarbeit</td>
  </tr>
     <tr>
    <th>Abgabe</th>
    <td>Im eigenen Repository (Siehe Abschnitt Resultat pro / Aufgabe separat geregelt)</td>
  </tr>
        <tr>
    <th>Typ</th>
    <td>Praktisch auf Ubuntu Server</td>
  </tr>
      </tr>
        <tr>
    <th>Erforderlich</th>
    <td>8a Ja, 8b-c Nein</td>
  </tr>
      </tr>
        <tr>
    <th>Punkte</th>
    <td>3 x 1 Punkt</td>
  </tr>
</table>





## Aufgabe 8a: WordPress installieren

 

Laden Sie den WordPress Core unter https://wordpress.org/ herunter und entpacken Sie die Dateien

Laden Sie die Files via FTP auf Ihren Host in das Verzeichnis des virtuellen Hosts

Führen Sie das Setup von WordPress aus, indem Sie die URL Ihren virtuellen Host im Browser aufrufen, Geben Sie beim Setup von WordPress die korrekten Datenbank Informationen an

 

**Hinweise**:

- https://wordpress.org/download/
- Wenn beim WordPress-Setup die Meldung erscheint, dass die Datei wp-config.php nicht erstellt werden kann, dann liegt es daran, dass die Berechtigungen auf dem virtuellen Host auf den Benutzer www-data angepasst werden müssen
  - chown -R www-data:www-data /pfad/zum/virtuellen/Host

- Geben Sie Ihrer WordPress-Installation einen beliebeigen Titel etc.
- Speichern Sie den WordPress-Admin Benutzer im Portfolio ab

**Resultat:**

- Melden Sie sich am Backend von WordPress an und erstellen Sie einen Printscreen inkl. Browser-Adressleiste

 

## Aufgabe 8b - WordPress Konfiguration

 

Definieren Sie folgende WP-Konstanten

- WP_MEMORY_LIMIT: 256M
- WP_DEBUG: true
- WP_DEBUG_LOG: true
- Deaktivieren Sie zusätzlich alle automatischen Updates
- Setzen Sie eine Administratoren-E-Mail-Adresse in den WordPress Einstellungen

**Hinweise:**

- https://brianli.com/the-difference-between-wp_memory_limit-and-wp_max_memory_limit/
- https://wordpress.org/documentation/article/debugging-in-wordpress/
- https://codex.wordpress.org/de:Automatische_Hintergrund_Updates_einstellen



**Resultat:**

- Gehen Sie im WordPress backend auf Werkzeuge – Website-Zustand – Bericht und wählen Sie den Button «Bericht in die Zwischenablage kopieren». Kopieren Sie den Inhalt in eine Textdatei und speichern Sie diese die in Ihrem Kanal ab




## Aufgabe 8c:  Fehler korrigieren

 

Wenn Sie nun auf Werkzeuge – Website-Zustand gehen werden Sie dort wohl die folgende Fehlermeldung sehen. 

#### Deine Website konnte eine Loopback-Anfrage nicht abschliessen

Korrigieren Sie den Fehler



**Hinweise**:

- Keine Hinweise



**Resultat:**

- Erstellen Sie einen Printscreen des Status, dass Sie den Fehler korrigiert, haben

 

 
