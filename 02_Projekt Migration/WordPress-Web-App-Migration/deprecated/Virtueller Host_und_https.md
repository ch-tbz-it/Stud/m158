# Auftrag 4 - Virtueller Host und HTTPS

<table>
  <tr>
    <th>Dauer</th>
    <td>90 Minuten </td>
  </tr>
  <tr>
    <th>Arbeit</th>
    <td>Einzelarbeit</td>
  </tr>
     <tr>
    <th>Abgabe</th>
    <td>Im eigenen Repository (Siehe Abschnitt Resultat pro / Aufgabe separat geregelt)</td>
  </tr>
        <tr>
    <th>Typ</th>
    <td>Praktisch auf Ubuntu Server</td>
  </tr>
      </tr>
        <tr>
    <th>Erforderlich</th>
    <td>Ja</td>
  </tr>
      </tr>
        <tr>
    <th>Punkte</th>
    <td>3 x 1 Punkt</td>
  </tr>
</table>




## Aufgabe 4a: index.html verschieben

Verschiebe die Apache Default index.html vom aktuellen Default Webroot Verzeichnis in einen Unterordner /var/www/htm/public

Zugriff über: Wie erfolgt nun der Zugriff über https neu auf die Index.html?

**Hinweise:**

- mkdir
- mv
- [Beispielbild](https://gitlab.com/ch-tbz-it/Stud/m158/-/blob/main/04_Unterrichtsressourcen/02_Bilder/Apache2%20default.png)

**Resultat:** 

- Speichern Sie einen Printscreen vom Zugriff auf den neuen Pfad der index.html ab





## Aufgabe 4b: Zusätzlicher virtual Host einrichten

Konfigurieren Sie einen virtuellen host unter Apache2, welcher unter einem von Ihnen eindeutigen Namen erreichbar ist und legen Sie dort eine neue Index.html Datei ab, welche den folgenden Inhalt besitzt.

`<!DOCTYPE html>`

`<head>`

<meta charset="utf-8">

`</head>`

`<html>`

`<h1>Hier wird später Ihre WordPress Umgebung installiert</h1>`

`</html>`



**Hinweise:**

- Wählen Sie einen Domainname, welcher öffentlich nicht besetzt ist
- Da wir ohne DNS arbeiten, müssen Sie einen Eintrag in Ihrer Hostdatei auf dem lokalen Rechner machen, mit welchem Sie auf den neuen virtuellen Host zugreifen, möchten
- https://www.thegeekstuff.com/2011/07/apache-virtual-host/ (Diesen Link nur verwenden, um nachzulesen was virtuelle Hosts sind. Zum Einrichten den Link von digitalocean verwenden)
- https://www.digitalocean.com/community/tutorials/how-to-set-up-apache-virtual-hosts-on-ubuntu-20-04
- apache2ctl -t (check)

 

**Resultat:** 

- Speichern Sie die den Namen über welcher der Virtual Host erreichbar ist
- Speicher Sie die Einstellungen für Ihren Virtual Host



## Aufgabe 4c: Self-Signed Zertifikat einrichten (HTTPS)

Damit später Ihre WordPress (Virtual Host) Umgebung über HTTPS aufgerufen werden kann müssen Sie ein SSL-Zertifikat erstellen und einrichten

 

 

**Hinweise:**

- https://www.digitalocean.com/community/tutorials/how-to-create-a-self-signed-ssl-certificate-for-apache-in-ubuntu-16-04

 

**Resultat:**

- Erstellen Sie einen Printscreen vom Zertifikat aus dem Browser
