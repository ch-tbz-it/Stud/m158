# Auftrag 7 - Installation Maria DB

<table>
  <tr>
    <th>Dauer</th>
    <td>90 Minuten </td>
  </tr>
  <tr>
    <th>Arbeit</th>
    <td>Einzelarbeit</td>
  </tr>
     <tr>
    <th>Abgabe</th>
    <td>Im eigenen Repository (Siehe Abschnitt Resultat pro / Aufgabe separat geregelt)</td>
  </tr>
        <tr>
    <th>Typ</th>
    <td>Praktisch auf Ubuntu Server</td>
  </tr>
      </tr>
        <tr>
    <th>Erforderlich</th>
    <td>7a-b Ja, 7c Nein</td>
  </tr>
      </tr>
        <tr>
    <th>Punkte</th>
    <td>3 x 1 Punkt</td>
  </tr>
</table>




## Aufgabe 7a: Maria DB

 

Installieren Sie MariaDB

Nach der Installation führen Sie den Befehl «mysql_secure_installation» aus. Damit Sie den Befehl korrekt ausführen können, müssen Sie zuerst den Authentifizierungstyp auf «mysql_native_password» setzen. Mehr dazu im Link von Stackoverflow Beitrag.

 

**Hinweise**:

- https://www.hostnextra.com/kb/how-to-install-apache-mariadb-php-and-phpmyadmin-in-ubuntu-18-04/ (Nur Schritt 3)
- https://stackoverflow.com/questions/39281594/error-1698-28000-access-denied-for-user-rootlocalhost

**Resultat:**

- Machen Sie einen Printscreen von dem folgenden Query
  

  ```
  SELECT User, Host, plugin FROM mysql.user; 
  ```

  (plugin='mysql_native_password)

 

## Aufgabe 7b: Installation «phpmyadmin»

 

Installieren Sie phpmyadmin inkl. Setup, fügen Sie den «include» Befehl in Ihre Apache2 Config ein und öffnen Sie phpmyadmin über den Browser.

**Hinweise:**

- Schauen Sie, dass für die phpmyadmin Installation der mysql User «root» kein Passwort Passwort also «blank» hat. Setzen Sie nach der Installation von phpmyadmin das PW wieder mit dem Assistenten «mysql_secure_installation»

  [**https://www.hostinger.com/tutorials/how-to-install-and-setup-phpmyadmin-on-ubuntu**](https://www.hostinger.com/tutorials/how-to-install-and-setup-phpmyadmin-on-ubuntu)

   

  [**https://stackoverflow.com/questions/26891721/phpmyadmin-not-found-after-install-on-apache-ubuntu**](https://stackoverflow.com/questions/26891721/phpmyadmin-not-found-after-install-on-apache-ubuntu)

  

  ```
  mysql -uroot -p
  ```

  (Login in mysql mit native Password)

  ```
  SET PASSWORD FOR root@localhost=PASSWORD('');
  ```

  (Passwort setzen in mysql für User root)

  ```
  flush privileges;
  ```

  (Neues Passwort wird aktiviert)

 

**Resultat:**

- Erstellen Sie einen Printscreen vom erfolgreichen Aufruf der «phpmyadmin» Seite.




## Aufgabe 7c:  MySQL Benutzer

 

- Erstellen Sie einen separaten MYSQL Admin-Benutzer für die Verwaltung von phpmyadmin
- Erstellen Sie eine leere Datenbank mit dem Namen «wordpress»
- Erstellen Sie einen Benutzer wordpress, welcher nur Zugriff auf die Datenbank wordpress hat

**Hinweise**:

- CREATE USER "admin"@"localhost" IDENTIFIED BY "Pass";
- GRANT ALL PRIVILEGES ON *.* TO "admin"@"localhost" WITH GRANT OPTION;




**Resultat:**

- Melden Sie sich mit dem Benutzer «admin» an phpmyadmin an und erstellen Sie einen Printscreen von den Datenbanken
- Melden Sie sich mit dem Benutzer wordpress an phpmyadmin an un erstellen Sie einen Printscreen von den Datenbanken

 

 
