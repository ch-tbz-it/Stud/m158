# Auftrag 9 - WordPress Plugins

<table>
  <tr>
    <th>Dauer</th>
    <td>60 Minuten </td>
  </tr>
  <tr>
    <th>Arbeit</th>
    <td>Einzelarbeit</td>
  </tr>
     <tr>
    <th>Abgabe</th>
    <td>Im eigenen Repository (Siehe Abschnitt Resultat pro / Aufgabe separat geregelt)</td>
  </tr>
        <tr>
    <th>Typ</th>
    <td>Praktisch auf Ubuntu Server</td>
  </tr>
      </tr>
        <tr>
    <th>Erforderlich</th>
    <td>Nein</td>
  </tr>
      </tr>
        <tr>
    <th>Punkte</th>
    <td>3 x 1 Punkt</td>
  </tr>
</table>






## Aufgabe 9a: SMTP-Plugin einrichten 

 

Installieren Sie das Plugin Easy WP SMTP (Der Plugin-Autor heisst identisch)

Richten Sie den entsprechende SMTP-E-Mail-Account ein, die Angaben finden Sie unter Hinweise (Alternativ können Sie auch Ihren eigenen SMTP-Account verwenden)

Nutzen Sie die Funktion “Send a Test” um einen Test durchzuführen

 

**Hinweise**:

```
**Verbindungseinstellungen**

E-Mail & Username:          [m158@geekz.ch](mailto:m158@geekz.ch)
Pass:                        m158.tbz123!
SMTP-Server                  asmtp.mail.hostpoint.ch
SMTP-Authentifizierung      Muss zwingend aktiviert sein
Username                     ihr.name@ihre-eigene-domain.ch
Port        587 unverschlüsselt oder verschlüsselt mit STARTTLS
Port        465 verschlüsselt mit SSL
```

**Achtung: Verwenden Sie diese E-Mail nur zum Versenden von Mails, nicht zum Empfangen!** 

**Resultat:**

- Speichern Sie das Testmail mit dem Namen “Testmail.msg” in Ihr Teams Kanal ab. Dabei muss ersichtlich sein, dass das E-Mail an Ihre Adresse gesendet wurde.

 

## Aufgabe 9b - Backup Plugin einrichten

 

Backup Plugin installieren und einrichten



**Hinweise:**

- Installieren Sie das Plugin BackWPUp
- Suchen Sie nun das neue Plugin innerhalb von WordPress
- Erstellen Sie einen regelmässigen Backup-Job, welcher jede Nacht um eine Zeit Ihrer Wahl zwischen 23.00 Uhr und 05 Uhr ein Backup erstellt. Dabei sollen die WordPress-Dateien und das Datenbankbackup in ein Zip File gespeichert werden
- Entfernen Sie die Option, dass nur ein E-Mail Log gesendet wird, wenn Fehler auftreten
- Führen Sie den Backup Job manuell aus



**Resultat:**

- Speichern Sie das E-Mail [SUCCESSFUL] -E-Mail mit dem Namen “Backup.msg” in Ihrem Teams Kanal ab




## Aufgabe 9c:  Was ist ein Theme & Child Theme

 

Lesen Sie die beiden WordPress Artikel unter Hinweise und erstellen Sie anschliessend ein Child-Theme mit einer separaten “”style.css Datei für das Theme “Twenty Twenty-Three". Nennen Sie Ihr neues Child-Theme gleich wie Ihren virtuellen Host.



**Hinweise**:

- https://developer.wordpress.org/themes/getting-started/what-is-a-theme/#required-files
- https://developer.wordpress.org/themes/advanced-topics/child-themes/



**Resultat:**

- Gehen Sie auf Werkzeuge -> Theme-Datei-Editor und wählen Sie dort Ihr Child Theme aus. Machen Sie einen Printscreen von ganzen damit man erkennen kann welche Dateien in Ihrem Child Theme enthalten sind.

 

 
