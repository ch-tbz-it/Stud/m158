# Auftrag 11 - WordPress Hack

<table>
  <tr>
    <th>Dauer</th>
    <td>60 Minuten </td>
  </tr>
  <tr>
    <th>Arbeit</th>
    <td>Einzelarbeit</td>
  </tr>
     <tr>
    <th>Abgabe</th>
    <td>Im eigenen Repository (Siehe Abschnitt Resultat pro / Aufgabe separat geregelt)</td>
  </tr>
        <tr>
    <th>Typ</th>
    <td>Praktisch auf Ubuntu Server</td>
  </tr>
      </tr>
        <tr>
    <th>Erforderlich</th>
    <td>Ja</td>
  </tr>
      </tr>
        <tr>
    <th>Punkte</th>
    <td>3 x 1 Punkt</td>
  </tr>
</table>

## Aufgabe 11a: MD5 Hash ersetzen 

 

- Nachdem Restore einer WordPress-Umgebung wissen Sie das Passwort des Administrators nicht mehr
- Ersetzen Sie in der Tabelle «wp_users» den MD5 Hash des Benutzers «admin» und melden Sie sich anschliessend an WordPress an

 **Hinweise**:

- https://codebeautify.org/md5-hash-generator

**Resultat:**

- Erstellen Sie einen Printscreen wo man sieht dass das Divi Theme installiert ist und Sie mit dem «admin» Benutzer angemeldet sind 



## Aufgabe 11b - Suchen & ersetzen

 

- Auf der Startseite sollte oben links das Logo angezeigt werden
- Da der Pfad des Bilder noch auf die URI der alten Umgebung zeigt kann das Bild nun nicht korrekt dargestellt werden
- Installieren Sie das Plugin WP Migrate Lite und führen Sie ersetzen suchen über die ganze WordPress Datenbank aus (Suchen = alte Domain / ersetzen = neue Domain) 

**Hinweise:**

- https://de.wordpress.org/plugins/wp-migrate-db/



**Resultat:**

- Erstellen Sie einen Printscreen von der Startseite mit dem korrekt angezeigten Logo
- Kopieren Sie den korrigierten Pfad vom Logo in Ihr Repository




## Aufgabe 11c:  Divi

-  Die neue WordPress Umgebung verwendet nun das Theme «Divi». Gehen Sie im WordPress unter Divi -> Support Center und überprüfen Sie dort den System Status
- Korrigieren Sie alle bemängelten Hinweise 

**Hinweise**:

- https://www.interserver.net/tips/kb/change-php-version-apache-ubuntu/



**Resultat:**

- Erstellen Sie einen Printscreen von Divi -> Support Center von dem «Full Report» (Es sollte natürlich alles grün sein)

 

 
