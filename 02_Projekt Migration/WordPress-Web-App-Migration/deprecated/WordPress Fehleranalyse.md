# Auftrag 12 - WordPress Fehleranalyse

<table>
  <tr>
    <th>Dauer</th>
    <td>120 Minuten </td>
  </tr>
  <tr>
    <th>Arbeit</th>
    <td>Einzelarbeit</td>
  </tr>
     <tr>
    <th>Abgabe</th>
    <td>Im eigenen Repository (Siehe Abschnitt Resultat pro / Aufgabe separat geregelt)</td>
  </tr>
        <tr>
    <th>Typ</th>
    <td>Praktisch auf Ubuntu Server</td>
  </tr>
      </tr>
        <tr>
    <th>Erforderlich</th>
    <td>Ja</td>
  </tr>
      </tr>
        <tr>
    <th>Punkte</th>
    <td>3 x 1 Punkt</td>
  </tr>
</table>


## Aufgabe 12a: MD5 Hash ersetzen 

 

- Nun geht es darum, Fehler in WordPress zu finden. Laden Sie zu diesem Zweck erneut ein WP-Backup zurück.

​		Download: [Restore-WP-M158-with-Errors.zip](https://tbzedu-my.sharepoint.com/:u:/g/personal/jonas_aeschlimann_tbz_ch/EXx8CqvU9EpKjvkoYZANO4gBhr_OGcrMcNhC3e0nQxNHgQ?e=hxfsKb)

- Nachdem Restore des Backups wird eine Fehlermeldung erscheinen, wenn Sie versuchen WordPress zu starten
- Kopieren Sie das error.log File vom Apache Dienst in Ihren privaten Teams Kanal 
- Lösen Sie den Fehler

**Hinweise**:

- Keine

**Resultat:**

- error.log
- Machen Sie einen Printscreen von der Startseite, welche nun wieder funktioniert



## Aufgabe 12b - Child-Theme Fehler

Download: [M158.zip](https://tbzedu-my.sharepoint.com/:u:/g/personal/jonas_aeschlimann_tbz_ch/EacgeBZlrzVKpR_VEUzkQRAB0nLhPvMsVytFqM-ApN7vng?e=IJzvQZ)

- Versuchen Sie nun das folgende Child Theme auf der Umgebung zu installieren
- Sie werden die Meldung erhalten: Das Eltern-Theme konnte nicht gefunden werden. Du musst das Eltern-Theme installieren
- Das Divi, welches das «Parent Theme» ist, ist installiert 
- Finden Sie heraus was das Problem ist und bringe das Child Theme zum laufen

**Hinweise:**

- wp_options / template



- **Resultat:**
  - Erstelle einen Printscreen vom funktionierenden Child-Theme




## Aufgabe 12c:  Google Analytics Tracking Code via WP Hooks

-  Erstellen Sie in Ihrem Child-Theme in der functions.php Datei eine PHP-Funktion, welche einen Google Analytics Tracking Code einbindet
-  Führen Sie die Funktion für den wp_head Hook aus

```
<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-5s68VJ6EYG1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-Z5SVJ6EYG1');
</script>
```

**Hinweise**:

- https://developer.wordpress.org/reference/hooks/wp_head/



**Resultat:**

- Erstellen Sie einen Printscreen vom Hook in der functions.php Datei und speichern Sie diesen
- Speichern Sie Ihren Quellcode vom <head> Bereich als Bild ab und markieren Sie den Code, welcher nun über den Hook ausgegeben wird im Quelltext

 

 
