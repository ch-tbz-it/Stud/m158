# Eigenes Migrationsprojekt

## Ausgangslage

Sie planen ein Migrationsszenario für eine Software-, Datenbank-, File- oder Systemmigration. Jede Migration beinhaltet ein Legacy-System und ein Zielsystem. Die Herausforderung besteht darin, das Legacy-System realistisch zu simulieren oder vorab einzurichten. Ideal wäre es, die Ausgangslage von einer bestehenden Quelle zu erhalten, beispielsweise Ihrem Lehrbetrieb oder einem vorhandenen System, das Sie migrieren möchten. Die Möglichkeiten sind äusserst vielfältig!

### Migration-Ideen

Windows Server und Active Directory wird im M159 ein umfangreiches Thema sein. Daher dürften Sie kein Projekt wählen dass in diesem Zusammenhang steht. Ansonsten sind Sie frei.

- OS-Migration von Windows zu Linux
- Umzug einer komplexen Applikation von A nach B
- Virtualisierung einer oder mehreren physischen Maschinen in eine Typ A Virtualisierung
- Migration von klassischen Serverdiensten (DHCP/File/Print)
- Portierung einer lokalen Anwendung in die Cloud
- Eine Datenmigration von Windows nach Linux mit komplexen Berechtigungen
- Migration eines Hardware Firewall/Routers zu einer Virtualisierung
- Datenbankmigration (z.B. von MS SQL nach MySQL)
- Ihre eigene Idee!

## Ziele & Bewertung

Da Sie Ihr Projekt selber wählen und definieren können, ist es nicht möglich und auch unsinnig, hier ein Bewertungsraster anzuwenden. Stattdessen werden Kriterien und Ziele definiert, die für die Bewertung relevant sind. 

- Aufwand welcher durch die Lernenden betrieben wurde
- Anzahl verschiedener Komponenten
- Komplexität der Migration und der eingesetzten Systeme

## Vorgehen

1. Bilden Sie 2er oder 3er. Alternativ dürfen Sie auch alleine arbeiten

2. Erstellen Sie einen Projektauftrag und ein Architekturdiagramm für Ihr Migrationsprojekt. Besprechen Sie diese Dokumente mit Ihrer Lehrkraft und legen Sie Ihre Ziele für die Evaluation fest.

3. Erstellen Sie einen Projektplan für die Migration. Ihr Projektplan sollte verschiedene Phasen der Migration beinhalten. Versuchen Sie eine gelungene Mischung zwischen Grob- und Detailplan zu finden.

   - Ihr Projektplan muss noch keine technischen Spezifikationen enthalten. Stattdessen wäre eine Phase wie z.B. Software-Evaluation sinnvoll, in der Sie klären, welche Software Sie später einsetzen werden. Vom Betriebssystem über die Datenbank bis hin zum Webserver. Wenn Sie bereits Erfahrung mit Software in diesem Bereich haben, liegt es an Ihnen, ob Sie darauf aufbauen oder etwas Neues lernen wollen.

   - Projektpläne erfordern viel Erfahrung, die Sie wahrscheinlich noch nicht haben. Diskutieren Sie Ihren Plan in der Klasse oder mit dem Lehrer, um zusätzliche Ideen und Anregungen zu erhalten. 

   - In Ihrem Projektplan sollten Sie auch eine zeitliche Einteilung vornehmen. Neben den theoretischen Inputs während des Kurses haben Sie bis einschließlich Tag 9* dieses Moduls Zeit für Ihre Migration.  *Bei verkürzten Quartelen haben Sie nur bis Tag 8 oder 7 Zeit.

4. Halten Sie die wichtigsten Erkenntnisse in einem Arbeitsjournal fest. Dabei geht es nicht darum, alle Arbeitsschritte zu dokumentieren (natürlich können Sie das), sondern wichtige "Aha"-Erlebnisse festzuhalten. Sie arbeiten in Ihrem eigenen Tempo und gemäß Ihrem Projektplan. 

5. Wenn Sie trotz Google, ChatGPT und anderen Hilfsmitteln an einer Stelle länger als 20 Minuten nicht weiterkommen, suchen Sie sofort Hilfe. Die Lehrkraft wird Ihnen gerne weiterhelfen.

6. Dokumentieren Sie Ihr Projekt

7. Das Ergebnis, die Dokumentation, die Projektplanung, das Lernjournal und das Fachgespräch sind Teil der Bewertung von LB2.

### GitLab User

- Jonas Aeschlimann: jonas.aeschlimann@tbz.ch @jonas_aeschlimann_tbz
