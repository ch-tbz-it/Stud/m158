![Logo](x_gitressourcen/ZH_logo.jpg)
# M158 - Software-Migration planen und durchführen

## Version der Modulidentifikation

*13.08.2021, V3 ([Modulbaukasten ICT-BCH](https://www.modulbaukasten.ch/module/158/3/de-DE?title=Software-Migration-planen-und-durchf%C3%BChren/)).*

*[Kompetenzmatrix – Modul 158](https://gitlab.com/modulentwicklungzh/cluster-org/m158/-/tree/master/1_Kompetenzmatrix)*



<hr >

## Kurzbeschreibung des Moduls gemäss Modulidentifikation

*Im Modul 158 setzen wir uns mit den Grundlagen der Software-Migration auseinander. Dabei werden wir uns mit der Theorie von Migrationsansätzen und praktischen Übungen unter Linux auseinandersetzen. Ergänzt wird der Unterricht mit Grundlagen, welche passend zum Thema Softwaremigration sind.*

<hr >

## Aufbau der Unterlagen zum Modul

Das Modul 158 besteht aus einem Theorie- sowie einem praktischen Teil. Das Modul ist in folgenden Elementen aufgebaut

- Theorie / Vorträge / Inputs
- Übungen
- Theorietest (LB1)
- Projekt Migration (LB2)

<hr >

### Umsetzungsvorschlag

[Umsetzungsvorschlag](https://gitlab.com/ch-tbz-it/Stud/m158/-/tree/main/03_Umsetzungsvorschlag) mit möglichem Modulablauf



<hr >

### Unterrichtsressourcen

[Unterrichtsressourcen](https://gitlab.com/ch-tbz-it/Stud/m158/-/tree/main/04_Unterrichtsressourcen) Übungen und Inhalten zu den einzelnen Kompetenzen



