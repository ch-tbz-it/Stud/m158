# Umsetzungsvorschlag - Modul 158

## Lektionen Plan

| Tag | Themen                   | Unterlagen / Übungen | Hausaufgaben |
|---|---------------------------|-------------|-----------------------------------------|
| 1 | - Einführung Software & Migration<br>- Architekturdiagramm<br>- Start Projekt Migration | - [Software.pptx](../04_Unterrichtsressourcen/01_Präsentationen)<br><br> | Repository M158 erstellen und Grundstruktur anlegen (Siehe Auftrag Migration) |
| 2 | - Vorstellung Glossar<br>- On-prem/-site,SAAS,IAAS, PAAS <br>- Berechtigungen unter Linux | - [Glossar](https://gitlab.com/ch-tbz-it/Stud/m158/-/blob/main/04_Unterrichtsressourcen/Glossar.md)<br>- [Berechtigungen_Linux_POSIX.pptx](https://gitlab.com/ch-tbz-it/Stud/m158/-/tree/main/04_Unterrichtsressourcen/01_Pr%C3%A4sentationen) <br>- Übung Berechtigungen unter Linux (folgt) | - Glossar für Prüfung |
| 3 | - Übung UNC Pfade <br>- Übung Absolute und relative Pfade<br>- Unterschiede der Berechtigungen unter Windows und Linux | - [Übung UNC.pdf](https://gitlab.com/ch-tbz-it/Stud/m158/-/tree/main/04_Unterrichtsressourcen/03_%C3%9Cbungen/UNC?ref_type=heads)<br/>[- Übung Pfade](https://gitlab.com/ch-tbz-it/Stud/m158/-/tree/main/04_Unterrichtsressourcen/03_%C3%9Cbungen/Pfade?ref_type=heads)<br>- [Unterschiede Pfade Linux / Windows](https://gitlab.com/ch-tbz-it/Stud/m158/-/blob/main/04_Unterrichtsressourcen/Unterschiede_Pfade_Windows_Linux.md?ref_type=heads) | - Glossar für Prüfung |
| 4 | - Übung CMS<br> | - [Übung CMS](https://gitlab.com/ch-tbz-it/Stud/m158/-/tree/main/04_Unterrichtsressourcen/03_%C3%9Cbungen/CMS-DB-connect) | - Für Theorietest lernen |
| 5 | - Übung - Software-Migrationsbeispiel<br>- Projekt Migration | Selbstständiges Arbeiten<br>[Übung - Software-Migrationsbeispiel](https://gitlab.com/ch-tbz-it/Stud/m158/-/blob/main/04_Unterrichtsressourcen/03_%C3%9Cbungen/%C3%9Cbung%20-%20Software-Migrationsbeispiel.md) | - Für Theorietest lernen |
| 6 | **- LB1 Theorietest**<br>Übung - Migrationsansätze<br>- Projekt Migration | Selbstständiges Arbeiten<br>[Übung - Migrationsansätze](https://gitlab.com/ch-tbz-it/Stud/m158/-/blob/main/04_Unterrichtsressourcen/03_%C3%9Cbungen/%C3%9Cbung%20-%20Migrationsans%C3%A4tze.md) |  |
| 7 | - Projekt Migration | Selbstständiges Arbeiten |  |
| 8 | - Projekt Migration | Selbstständiges Arbeiten |  |
| 9 | - Projekt Migration | Selbstständiges Arbeiten |  |
| 10 | - Projekt Migration<br>- Finale Abgabe Dokumentation | Selbstständiges Arbeiten | - Wer die Dokumentation der LB2 bereits am Tag 9 oder früher abgegeben hat, kennt seine Modulnote vorzeitig und kann gegebenenfalls Korrekturen angeben. |
